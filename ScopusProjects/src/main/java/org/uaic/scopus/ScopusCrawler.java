package org.uaic.scopus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ScopusCrawler {
	
	public static void main(String[] args) throws Exception {
		CrawlScopus();
	}
	
	private static void CrawlScopus()
	{
		String baseUrl = "https://www.scopus.com/search/form.uri?display=authorLookup&clear=t&origin=searchbasic";		
		try {
			GetSearchResults(baseUrl, 200, "smith");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static String GetSearchResults(String baseUrl, Integer resultsPerPage, String author) throws ClientProtocolException, IOException, URISyntaxException
	{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		URIBuilder uriBuilder;
		String newLocation;
		String responseStr;
		int statusCode;
		
		// Open url
		HttpGet httpGet = CreateHttpGet(baseUrl);
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		statusCode = httpResponse.getStatusLine().getStatusCode();
		System.out.println(statusCode);
		newLocation = GetLocation(httpResponse.getAllHeaders());
		
			
		Integer offSet = 1;
		//String results ="";
		
		//String stepResults;
		String csv = "author,subject area,affiliation,city,country/territory\n";
		do{			
			//replace
			baseUrl=baseUrl.replace("search/form.uri?display=authorLookup&clear=t&origin=searchbasic", "results/authorNamesList.uri?origin=searchauthorlookup&");
			uriBuilder = new URIBuilder(baseUrl);
		    uriBuilder.setParameter("st1", author);
			uriBuilder.setParameter("resultsPerPage", resultsPerPage.toString());
			uriBuilder.setParameter("offSet", offSet.toString());
			
			httpGet = new HttpGet(uriBuilder.build().toString());
			httpResponse = httpClient.execute(httpGet);
			System.out.println("Response Status: " + httpResponse.getStatusLine().getStatusCode());
			String encoding = GetEncoding(httpResponse.getAllHeaders());
			responseStr = GetResponseBody(httpResponse, encoding);
			
			//String pageUri = uriBuilder.build().toString();
			Document doc = Jsoup.parse(responseStr);
			Elements articleEntries = doc.select("tr.searchArea");
			
			for (Element el : articleEntries)
			{
				String autor = el.select("span.docTitle").text().trim();	
				String subj = el.select("tr.dataCol4").text().trim();	
				String afiliere = el.select("tr.dataCol5").text().trim();
				String oras = el.select("tr.dataCol6").text().trim();
				String tara = el.select("tr.dataCol7").text().trim();
							
				csv += ("\"" + autor + "\",\"" + subj + "\",\"" + afiliere + "\",\"" + oras + "\",\"" + tara + "\"\n");
			}	

			offSet+=200;
		} while (offSet < 4002);
		
		WriteToFile(csv, "Scopus.csv");
		
		httpClient.close();		
		return csv;
	}
	
	private static HttpGet CreateHttpGet(String url)
	{
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("Connection", "keep-alive");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");		
		httpGet.addHeader("Accept-Encoding", "gzip, deflate");
		httpGet.addHeader("Accept-Language", "en-US,en;q=0.5");
		
		return httpGet;
	}
	
	private static String GetResponseBody(CloseableHttpResponse httpResponse, String encoding) throws IllegalStateException, IOException
	{
		InputStream is = httpResponse.getEntity().getContent();
		String body = IOUtils.toString(is, encoding);
		is.close();
		//reader.close();
		return body; //response.toString();
	}
	
	private static String GetLocation(Header[] responseHeaders)
	{
		for(Header header: responseHeaders)
			if (header.getName().equals("Location"))
				return header.getValue();
		return "";
	}
	
	private static String GetEncoding(Header[] responseHeaders)
	{
		for(Header header: responseHeaders)
			if (header.getName().equals("Content-Type"))
			{
				String encoding = header.getValue();
				return encoding.substring(encoding.indexOf("charset=")+8).split(";")[0].trim();
			}
		return "";
	}
	

	private static void WriteToFile(String text, String fileName) {
		try {
			FileUtils.writeStringToFile(new File("rezultat/" + fileName), text, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	}


